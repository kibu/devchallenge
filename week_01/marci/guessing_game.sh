#!/bin/bash


declare -i secret=821
declare -i rd=0
declare -i cnt=0
declare -i flag=0
declare -i size=0

list=()

while [ $rd -ne $secret ]
do
	flag=0
	read rd

	for ((i=0;${#list[@]}>i;i++)) {
		if [[ $rd -eq ${list[$i]} ]]
		then
			flag=1				#vótmá
			break;
		fi
	}

#-------------------------------------------------------------------------------#
	if [ $flag -eq 0 ]				#nemvótmég
	then
		list=(${list[@]} $rd)			#append new value to the list
		cnt=$((cnt+1))				#increment the try-counter
	elif [ $flag -eq 1 ]
	then
		echo "You're already tried this number before"
		continue
	fi

#--------------------------------------------------------------------------------#
	if [ $rd -gt $secret ]
	then
		echo "Your number is greater than the secret number"
	elif [ $rd -lt $secret ]
	then
		echo "Your number is less than the secret number"
	else
		break
	fi
done

cnt=$((cnt-1))

echo "You're right! The secret number is $secret!"
echo "Your score: $cnt"
