#!/bin/bash

coworkers=("Vasvári Gábor" "Nemes Attila" "Balázs Eszter" "Balogh Lili" "Bodnár Ádám" "Hitér Bence" "Franyó Ádám" "Lukács Manuéla" "Makra Sándor" "Makrai Patrik" "Nagy Ani" "Tóth Márton" "Purcl András" "Varró Melinda" "Zsiga Bálint" "Gronki")

read

for i in ${!coworkers[*]}
do
	if [[ "${coworkers[$i]}" == *"$REPLY"* ]]
	then
		echo "Hello ${coworkers[$i]}!"
		exit
	fi
done
