fun main(args: Array<String>) {
    val guesses = mutableListOf<Int>()
    var guessed: Boolean = false
    val randomNumber = (1..100).random()

    while (!guessed) {
        val guess = readLine()
        val guessNumber = guess?.toInt()
        if (guessNumber!! < randomNumber) {
            print("random number is bigger\n")
            if (guessNumber !in guesses) guesses.add(guessNumber)
        } else if (guessNumber!! > randomNumber) {
            print("random number is lower\n")
            if (guessNumber !in guesses) guesses.add(guessNumber)
        } else {
            guessed = true
            print("guessed correctly, random number was: $randomNumber")
            print("you guessed it from ${guesses.size + 1} tries")
        }
    }
}

