val coworkerNames = arrayOf("Patrik", "Balint", "Adam", "Manu", "Sanya")

fun main(args: Array<String>) {
    print("Please enter your name \n")
    val name = readLine()
    if (name != null) {
        greet(name)
    }
}

fun greet(name: String) {
    if (name in coworkerNames)
        print("Hello $name")
    else
        print("Hello")
}