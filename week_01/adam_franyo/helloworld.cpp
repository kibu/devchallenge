// Hello World program in C++

#include<iostream>
using namespace std;

class HelloWorld{
	public:
		string mystring;
};

int main()
{
	HelloWorld myobj;

	myobj.mystring = "Hello World";

	cout << myobj.mystring << '\n';

	return 0;
}