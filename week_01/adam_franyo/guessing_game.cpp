// Hello World program in C++

#include<iostream>
#include<cstdlib>
using namespace std;

class Guessing_game{
	public:
		int mynumber;
};

int main()
{
	int number = -1;
	int counter = 0;
	int limit =30;
	int n[limit];

	for ( int i = 0; i < limit; i++ ) {
    	n[i] = -1;
    }

	Guessing_game myobj;
	srand(time(0));

	myobj.mynumber = rand() % 100;

	cout << "I thought about a number between 0 and 99. Guess my number please!" << '\n';

	while(number != myobj.mynumber){

		int found = 0;

		cin >> number;
		if(number < myobj.mynumber){
			cout << "My number is higher" << '\n';
		}
		if(number > myobj.mynumber){
			cout << "My number is lower" << '\n';
		}

		for(int i = 0; i < limit; i++){

			if (n[i] == number)
			{
				break;
			}
			if (i == limit-1){

				n[counter] = number;
				counter++;
			}
		}
	}
	if(number == myobj.mynumber){
		cout << "You win!" << '\n';
		cout << "Your number of attempts is: " << counter << '\n';
	}

	return 0;
}