// Hello World program in C++

#include<iostream>
#include <string.h>
using namespace std;

class Greeting{
	public:
		char mystring[10];
		char coworkers[10][10] = {"Adam", "Balint", "AdamB", "Ani", "Sanya", "Patrik", "Marci", "Bence", "Eszti", "Manu"};
};

int whoareyou(char worker[10], char coworkers[10][10]){
	int result = -1;

	for(int j = 0; j < 10; j++){
		if(strcmp(worker,coworkers[j]) == 0){
			result = 0;
		}
	}

	return result;
}

int main()
{
	int worker = -1;
	int counter = 0;

	Greeting myobj;

	cout << "What is your name?" << '\n';

	for(int j = 0; j < 3; j++){
		cin >> myobj.mystring;
		worker = whoareyou(myobj.mystring, myobj.coworkers);

		if(worker == 0){
			cout << "Hey " << myobj.mystring << "!" << '\n';
			break;
		}
		if(counter < 2){
			cout << "Sorry you are not a member of team! Try again!" << '\n';
			counter++;
		}
		else{
			cout << "Sorry you are disabled!" << '\n';
			break;
		}
	}

	return 0;
}