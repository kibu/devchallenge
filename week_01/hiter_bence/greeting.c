#include <stdio.h>
#include <string.h>
int main(void) {
  int i, found = 0;
  char name_list[20][20] = {
    "Bence",
    "Patrik",
    "Ádám",
    "Sanya",
    "Marci",
    "Ani",
    "Skeam",
    "Manu",
    "Gábor",
    "Vencel",
    "Bálint",
    "Eszter",
    "Meli",
    "Attila",
    "Lili",
    "Gronki"
  }, name[20];
  printf("Enter your name: ");
  scanf("%s", name);
  for (i = 0; i < 20; i++) {
    if (strcmp(name, name_list[i]) == 0) {
      found = 1;
      break;
    }
  }
  if (found == 1) {
    printf("Hello co-worker %s! \n", name);
  } else {
    printf("Bye stranger %s! \n", name);
  }
  return 0;
}
