#include <stdio.h>
#include <stdlib.h>
#include <time.h>
int main(void) {
  int random_num, guessed_num, counter = 0; 
  srand(time(NULL));
  random_num = rand() % 100 + 1;
  printf("Guess the number between 1 and 100: ");
  while(1) {
    counter++; 
    scanf("%d", &guessed_num);
    if (guessed_num == random_num) {
      if (counter < 7) {
        printf("Nice! \n");
      } else {
        printf("You can do better than this! \n");
      }
      printf("You guessed it in %d tries. \n", counter);
      break;
    } else {
      if (guessed_num < random_num) {
        printf("Higher! \n");
      } else {
        printf("Lower! \n");
      }
      printf("Guess again: ");
    }
  }
  return 0;
}