HELLO

If your language of choice doesn’t have a build in list and/or string type (e.g. you use C), these exercises should also be solvable for arrays. However, some solutions are very different between an array-based list (like C++’s vector) and a pointer based list (like C++’s list), at least if you care about the efficiency of your code. So you might want to either find a library, or investigate how to implement your own linked list if your language doesn’t have it.

Week 02:

1) list_manipulation:

Create a list with string in it.

a) Write a function that returns the largest element in a list.
b) Write function that reverses a list, preferably in place.
c) Write a function that checks whether an element occurs in a list.
d) Write a function that returns the elements positions in a list.

2) palindrome

Write a function that tests whether a string is a palindrome.

3) number_sums

Write three functions that compute the sum of the numbers in a list: using a for-loop, a while-loop and recursion. (Subject to availability of these constructs in your language of choice.)

4) two_lists

a) Write a function that concatenates two lists. [a,b,c], [1,2,3] → [a,b,c,1,2,3]
b) Write a function that combines two lists by alternatingly taking elements, e.g. [a,b,c], [1,2,3] → [a,1,b,2,c,3].
c) Write a function that merges two sorted lists into a new sorted list. [1,4,6],[2,3,5] → [1,2,3,4,5,6]. You can do this quicker than concatenating them followed by a sort.
d) Write a function that rotates a list by k elements. For example [1,2,3,4,5,6] rotated by two becomes [3,4,5,6,1,2]. Try solving this without creating a copy of the list. 

5) digits

Write a function that takes a number and returns a list of its digits. So for 2342 it should return [2,3,4,2].

have fun :))